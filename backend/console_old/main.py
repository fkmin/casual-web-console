from fastapi import FastAPI
from fastapi.responses import HTMLResponse, JSONResponse
import traceback

from casual.server.api import call, send, receive, start_server, Service
from casual.server.log import userlog
import os
import json
from time import sleep
app = FastAPI()




@app.get("/")
async def hello():
    return {"hello": "world"}

#userlog(b'hello python')

@app.get("/cas", response_class=JSONResponse)
async def test_casual():
    try:
        resp = call('.casual/domain/state', '{}')
        #userlog('hello')
    except Exception as e:
        trace = traceback.format_exc()
        print(trace)
        return f"""
        <h3 style="font-family: monospace;">{trace}</h3>
        """

    return json.loads(resp)
    #return resp.decode('utf-8')
    #return json.dumps(json.loads(resp))



@app.get("/byt", response_class=HTMLResponse)
async def test_casual_bytes():
    try:
        resp = send('.casual/domain/state', '{}')
        res = receive(resp)

    except Exception as e:

        trace = traceback.format_exc()
        print(trace)
        return f"""
        <h3 style="font-family: monospace;">{trace}</h3>
        """

    #return res.data()
    #return resp.decode('utf-8')
    return json.dumps(json.loads(res))
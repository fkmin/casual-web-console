import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()


setuptools.setup(
    name="casual-console",
    version="0.2.1",
    author="Mikael Ivarsson",
    author_email="mikael.ivarsson4@gmail.com",
    description="Web console for Casual",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="",
    python_requires=">3",
    packages=setuptools.find_packages(),
    classifiers=[
         "Programming Language :: Python :: 3",
         "Programming Language :: Python :: 3.6",
         "Programming Language :: Python :: 3.8",
         "License :: OSI Approved :: MIT License",
         "Operating System :: OS Independent",
    ],
    include_package_data=True,
    install_requires=[
        "fastapi>=0.63.0",
        "uvicorn>=0.13.4",
        "aiofiles>=0.6.0",
        "python-dotenv>=0.17.0"
    ]


)

from fastapi import APIRouter
from console.services.internal.information import CasualInternalService
from console.services.internal.dashboard.dashboard import CasualDashboardInformationService
from console.models.internal.dashboard.dashboard import Dashboard


import logging

router = APIRouter(
    prefix="/casual"
)


@router.get("/information", response_model=Dashboard)
async def get_v1():
    casual = CasualDashboardInformationService()
    dashboard = casual.get_dashboard()
    return dashboard


@router.get("/servers")
async def get_servers():
    casual = CasualInternalService()
    servers = casual.get_servers()
    return servers


@router.get("/services")
async def get_servers():
    casual = CasualInternalService()
    services = casual.get_services()
    return services


@router.get("/queues")
async def get_servers():
    casual = CasualInternalService()
    queues = casual.get_queues()
    return queues


@router.get("/groups")
async def get_servers():
    casual = CasualInternalService()
    servers = casual.get_groups()
    return servers

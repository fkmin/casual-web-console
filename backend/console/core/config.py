
from typing import List, Union

from pydantic import AnyHttpUrl, BaseSettings, validator



class Settings(BaseSettings):
    PROJECT_NAME: str = "CASUAL CONSOLE"
    BACKEND_CORS_ORIGINS: List[AnyHttpUrl] = []

    CASUAL_DOMAIN_STATE = ".casual/domain/state"
    CASUAL_SERVICE_STATE = ".casual/service/state"
    CASUAL_GATEWAY_STATE = ".casual/gateway/state"
    CASUAL_QUEUE_STATE = ".casual/queue/state"
    CASUAL_TRANSACTION_STATE = ".casual/transaction/state"

    @validator("BACKEND_CORS_ORIGINS", pre=True)
    def assemble_cors_origins(cls, v: Union[str, List[str]]) -> Union[List[str], str]:
        if isinstance(v, str) and not v.startswith("["):
            return [i.strip() for i in v.split(",")]
        elif isinstance(v, (list, str)):
            return v
        raise ValueError(v)

    

    class Config:
        case_sensitive = True
        env_file = ".env"


settings = Settings()

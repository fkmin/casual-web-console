from pydantic import BaseModel
from typing import Optional

class DDomain(BaseModel):
    group_count: int = 0
    server_count: int = 0
    executable_count: int = 0
    
class DServices(BaseModel):
    admin_count: int = -1
    admin_invoked_count: int = -1
    route_count: int = -1
    service_count: int = -1
    invoked_count: int = -1
    invoked_time: int = -1
    invoked_average: int = -1

class DQueue(BaseModel):
    qcount: int = -1
    gcount: int = -1

class Dashboard(BaseModel):
    version: str = "-1"
    name: str = "Noname"
    domain: DDomain = DDomain()
    services: DServices = DServices()
    queues: DQueue = DQueue()

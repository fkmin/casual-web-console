from pydantic import BaseModel
from typing import List
from console.models.internal.metrics import Metrics

class ServiceList(BaseModel): 
    admin_services: List[Service] = list()
    services: List[Service] = list()


class Service(BaseModel):
    name: str = ""
    category: str = ""
    metrics: Metrics = Metrics()
    instances: List[int] = list()
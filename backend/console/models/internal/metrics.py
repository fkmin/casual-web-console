
from pydantic import BaseModel


class Metric(BaseModel):
    invoked: Called = Called()
    pending: Called = Called()
    last_called: bigint = bigint()



class Called(BaseModel):
    count: int = -1
    total_time: int = -1
    min_time: int = -1
    max_time: int = -1
    avg_time: int = -1



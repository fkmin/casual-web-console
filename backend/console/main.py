import os
import sys
import uvicorn
from typing import Tuple, List
ENVIRONMENT_VARIABLES_NEEDED = [
    'CASUAL_HOME',
    'LD_LIBRARY_PATH',
    'CASUAL_DOMAIN_HOME'
]

def pre_init() -> Tuple[int, List]:

    init_code = 0
    missing = []
    for env in ENVIRONMENT_VARIABLES_NEEDED:
        check_env = os.environ.get(env, None)
        if check_env == None:
            missing.append(env)
            init_code = 2

    return init_code, missing




if __name__ == "__main__":
    init_code, missing = pre_init()
    if init_code != 0:
        print(f"Error: Environment not correctly set.\nMissing variable(s): [{', '.join(missing)}]")
        sys.exit(init_code)
    log_config = uvicorn.config.LOGGING_CONFIG
    log_config["formatters"]["access"]["fmt"] = "%(asctime)s - %(levelname)s - %(message)s"
    log_config["formatters"]["default"]["fmt"] = "%(asctime)s - %(levelname)s - %(message)s"
    uvicorn.run("console.server:get_application", host="0.0.0.0", port=8000, reload=True, debug=True, log_config=log_config, factory=True)
from typing import Dict
from console.services.outgoing.casual import CasualOutboundService


class CasualInternalService:

    def __init__(self):
        self._outbound = CasualOutboundService()

    def casual_information(self) -> Dict:
        resp = {}
        resp['domain'] = self._outbound.get_domain()
        resp['services'] = self.get_services(include_admin=True)
        resp['transactions'] = self._outbound.get_transactions()
        resp['gateway'] = self._outbound.get_gateway()
        resp['queues'] = self._outbound.get_queues()

        return resp

    @staticmethod
    def get_invoked_count(metric) -> int:
        count = 0
        try:
            count = metric['invoked']['count']
        except:
            pass
        return count


    def get_services(self, include_admin=False) -> Dict:
        raw_services = self._outbound.get_services()
        admin, nonadmin = [], []
        for service in raw_services:
            if include_admin and service.get('category', '') == '.admin':
                admin.append(service)
            else:
                nonadmin.append(service)
        services = {'services':nonadmin, 'admin_services':admin}
        return services

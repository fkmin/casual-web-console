from console.services.internal.information import CasualInternalService
from console.models.internal.dashboard.dashboard import Dashboard, DDomain, DServices, DQueue
from console.core.logging import get_logger

DOMAIN = "domain"
SERVICES = "services"
QUEUES = "queues"
GROUPS = "groups"

class CasualDashboardInformationService:
    def __init__(self):
        _internal_service = CasualInternalService()
        self._casual = _internal_service.casual_information()
        self._dashboard = Dashboard()
        self._error = []
        self._log = get_logger()

    def get_dashboard(self) -> Dashboard:
        self._casual_base()
        self._casual_domain()
        self._casual_services()
        self._casual_queues()
        return self._dashboard

    def _casual_base(self) -> None:
        try:
            raw_domain = self._casual.get(DOMAIN, {})
            version = raw_domain.get('version', {}).get('casual', '0')
            self._dashboard.name = raw_domain.get(
                'identity', {}).get('name', 'noname')
            self._dashboard.version = version
        except Exception as e:
            self._log.error(f'Error getting Casual Base {str(e)}')
            self._error.append("_casual_base")

    def _casual_domain(self) -> None:
        try:
            raw_domain = self._casual.get(DOMAIN, {})
            self._dashboard.domain.group_count = len(
                raw_domain.get('groups', []))
            self._dashboard.domain.server_count = len(
                raw_domain.get('servers', []))
            self._dashboard.domain.executable_count = len(
                raw_domain.get('executables', []))
            
        except:
            self._error.append("_casual_domain")
            pass
    
 
    def _casual_services(self) -> None:
        try:
            raw_services = self._casual.get(SERVICES, {})
            admin = raw_services.get('admin_services')
            services = raw_services.get('services')
            self._dashboard.services.admin_count = len(admin)
            self._dashboard.services.service_count = len(services)

            invoked_count = 0
            invoked_time = 0
            for service in services:
                metric = service.get('metric', {})
                invoked_count += self.get_invoked_attr(metric)
                invoked_time += self.get_invoked_attr(metric, "total")
            admin_invoked_count = 0
            for service in admin:
                admin_invoked_count += self.get_invoked_attr(
                    service.get('metric', {}))

            self._dashboard.services.admin_invoked_count = admin_invoked_count
            self._dashboard.services.invoked_count = invoked_count
            self._dashboard.services.invoked_time = invoked_time
            self._dashboard.services.invoked_average = self._average(invoked_time, invoked_count)
        except Exception as e:
            self._log.error(f'Error getting Dashboard information  | Casual Services |  {str(e)}')

            pass

    def _casual_queues(self)-> None:
        try:
            q = self._casual.get(QUEUES, {})
            self._dashboard.queues.gcount = len(q.get('groups'))
            self._dashboard.queues.qcount = len(q.get('queues'))

        except Exception as e:
            self._log.error(f'Error getting Dashboard information  | Casual Services |  {str(e)}')


    @staticmethod
    def get_invoked_attr(metric, attr="count") -> int:
        count = -1
        try:
            count = metric['invoked'][attr]
        except:
            pass
        return count

    @staticmethod
    def _average(arg1, arg2) -> int:
        if arg2 > 0:
            return arg1 / arg2
        else:
            return arg1

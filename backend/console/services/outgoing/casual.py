import json
from typing import Dict, List, Union
from casual.server.api import call

from console.core.config import settings
from console.core.logging import get_logger

logger = get_logger()


class CasualOutboundService:

    def __init__(self):
        self.domain = None
        self.services = None
        self.servers = None
        self.groups = None
        self.gateway = None
        self.transactions = None
        self.queues = None
        pass

    @staticmethod
    def format_raw_response(response: Dict, key: str = "result", return_type: Union[List, Dict] = dict()) -> Union[Dict, List]:
        return response.get(key, {} if isinstance(return_type, Dict) else [])

    def call_casual(self, service: str, input: str) -> Union[Dict, List]:
        logger.info(f"Call Casual outbound: {service}")
        resp = call(service, input)
        res = json.loads(resp)
        return res

    def get_domain(self) -> Dict:
        if not self.domain:
            res = self.call_casual(settings.CASUAL_DOMAIN_STATE, "{}")
            self.domain = self.format_raw_response(res)

        return self.domain

    def get_servers(self):
        if not self.servers:
            self.domain = self.get_domain()
            self.servers = self.format_raw_response(
                self.domain, "servers", list())
        return self.servers

    def get_groups(self):
        if self.groups:
            return self.groups
        domain = self.get_domain()
        return self.format_raw_response(self.domain, "groups", list())

    def get_services(self, limit: int=0):
        if not self.services:
            res = self.call_casual(settings.CASUAL_SERVICE_STATE, "{}")
            res = self.format_raw_response(res)
            self.services = self.format_raw_response(res, 'services', return_type=list())
        return self.services

    def get_gateway(self):
        if not self.gateway:
            res = self.call_casual(settings.CASUAL_GATEWAY_STATE, "{}")
            self.gateway = self.format_raw_response(res)
        return self.gateway


    def get_transactions(self):
        if not self.transactions:
            res = self.call_casual(settings.CASUAL_TRANSACTION_STATE, "{}")
            self.transactions = self.format_raw_response(res)
        return self.transactions

    def get_queues(self):
        if not self.queues:
            res = self.call_casual(settings.CASUAL_QUEUE_STATE, "{}")
            self.queues = self.format_raw_response(res)
        return self.queues

from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import HTMLResponse, JSONResponse, FileResponse
from fastapi.requests import Request
from fastapi.staticfiles import StaticFiles
import uvicorn
import os
from console.core.config import settings
from console.core.logging import get_logger
import pkg_resources

from v1.casual import api as casual

prefix = "/api/v1"

def get_application():

    log = get_logger()
    log.info("Starting Casual Console")
    _app = FastAPI(title=settings.PROJECT_NAME)

    _app.add_middleware(
        CORSMiddleware,
        allow_origins=[str(origin) for origin in settings.BACKEND_CORS_ORIGINS],
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )

    try:
        _app.mount("/webapp", StaticFiles(directory=pkg_resources.resource_filename(__name__, 'webapp')), name="webapp")
    except:
        pass
    
    _app.include_router(casual.router, prefix=prefix)

    
   # @_app.get("{full_path:path}", include_in_schema=False)
   # async def serve_webapp(request: Request, full_path: str):
   # # res = HTMLResponse(pkg_resources.resource_string(__name__, 'webapp/index.html'))
   #     res = FileResponse('webapp/index.html', media_type='text/html')
   #     return res

    @_app.exception_handler(Exception)
    async def handle_exceptions(request: Request, exc: Exception):
        return JSONResponse(
            status_code=500,
            content={"message": f'{exc}'}
        )

    log.info("Casual Console running...")


    return _app

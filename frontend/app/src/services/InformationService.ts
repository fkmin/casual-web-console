import http from "@/http.config";
import { DDashboard } from "@/models/dashboard";

class InformationServiceImpl {
  async getInformation(): Promise<DDashboard> {
    const result = await http.get("/api/v1/casual/information");
    return result.data;
  }
}

export const InformationService = new InformationServiceImpl();

export type DDashboard = {
  version: string;
  name: string;
  domain: DDomain;
  services: DServices;
  queues: DQueues;
};

export type DDomain = {
  group_count: number;
  server_count: number;
  executable_count: number;
};
export type DServices = {
  adminCount: number;
  adminInvokedCount: number;
  routeCount: number;
  service_count: number;
  invokedCount: number;
  invokedTime: number;
  invokedAverage: number;
};

export type DQueues = {
  qcount: number;
  groupCount: number;
};


export type DInformation = {
    servers: number;
    executables: number;
    services: number;
    groups: number;
    queues: number;
}
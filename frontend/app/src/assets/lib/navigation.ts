export const sidebarWidthToggle = function(): void {
  const sidenav: HTMLElement = document.getElementById(
    "sidenav"
  ) as HTMLElement;

  const collapsed: string = sidenav.getAttribute("data-collapse") as string;

  sidenav.setAttribute(
    "data-collapse",
    collapsed == "expanded" ? "collapsed" : "expanded"
  );
};

import { InjectionKey } from "vue";
import { createStore, Store, useStore as useDefaultStore } from "vuex";

import { InformationService } from "@/services";
import {DInformation} from "@/models/dashboard";
export interface State {
  version: string;
  domainName: string;
  serverCount: number;
  executableCount: number;
  groupCount: number;
  serviceCount: number;
  queueCount: number;
}

export const key: InjectionKey<Store<State>> = Symbol();

export const store = createStore<State>({
  state: {
    version: "0.1",
    domainName: "",
    serverCount: 0,
    executableCount: 0,
    groupCount: 0,
    serviceCount: 0,
    queueCount: 0
  },
  getters: {
    version: state => state.version,
    name: state => state.domainName,
    information: function(state): DInformation  {
      const info = {
        version: state.version,
        domainName: state.domainName,
        servers: state.serverCount,
        executables: state.executableCount,
        services: state.serviceCount,
        groups: state.groupCount,
        queues: state.queueCount
      };
      return info;
    }
  },
  mutations: {
    information: (state, payload) => {
      (state.version = payload.version),
        (state.domainName = payload.domainName),
        (state.serverCount = payload.servers),
        (state.executableCount = payload.executables),
        (state.serviceCount = payload.services),
        (state.groupCount = payload.groups),
        (state.queueCount = payload.queues);
    }
  },
  actions: {
    information: ({ commit }) => {
      InformationService.getInformation().then(info => {
        const payload = {
          version: info.version,
          domainName: info.name,
          servers: info.domain.server_count,
          services: info.services.service_count,
          groups: info.domain.group_count,
          executables: info.domain.executable_count,
          queues: info.queues.qcount
        };

        commit("information", payload);
      });
    }
  },
  modules: {}
});

export const useStore = function() {
  return useDefaultStore(key);
};
